/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id:coverpage
    property string time:"Spotra";
    property string distance:"0 km";
    Column{
        anchors.fill: parent
        Item{
            width:parent.width
            height: parent.height*0.1
        }
        Text {
            x:3
            id: timetext;
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            width:parent.width-6
            height:parent.height*0.4
            text: coverpage.time;
            color: Theme.primaryColor
            font.bold: true
            font.pixelSize: Theme.fontSizeLarge
            wrapMode: Text.WordWrap
        }
        Item{
            width:parent.width
            height: parent.height*0.1
        }
        Text {
            x:3
            id: distancetext;
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            width:parent.width-6
            height:parent.height*0.4
            text: coverpage.distance;
            color: Theme.primaryColor
            font.bold: true
            font.pixelSize: Theme.fontSizeExtraLarge
            wrapMode: Text.WordWrap
        }
        //Digit doesn't work, probably because of scaling.
        /*Row{
            width:parent.width
            height:parent.height*0.3
            spacing: 0
            Item{
                width:parent.width*0.05
                height: parent.height
            }
            Digit{
                id:decdigit;
                width:parent.width*0.4
                height: parent.height
                digittodisplay: coverpage.decsecs
                digitcolor: Theme.highlightColor
                digitlinewidth: parent.width*0.05
            }
            Item{
                width:parent.width*0.1
                height: parent.height
            }
            Digit{
                id:digit
                width:parent.width*0.4
                height: parent.height
                digittodisplay: coverpage.secs
                digitcolor: Theme.highlightColor
                digitlinewidth: parent.width*0.05
            }
            Item{
                width:parent.width*0.05
                height: parent.height
            }
        }*/
        Item{
            width:parent.width
            height: parent.height*0.1
        }
    }
}


