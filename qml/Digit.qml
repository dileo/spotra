/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
import QtQuick 2.0

Item {
    id:root
    property int digitlinewidth: 10
    property color digitcolor: 'steelblue'
    property color digitemptycolor:Qt.darker(digitcolor,2.0);
    property int digittodisplay:-1
    property int digitlineheighthorizontal:grid.width-(2*digitlinewidth)
    property int digitlineheightvertical:(grid.height-(3*digitlinewidth))/2
    Grid{
        id:grid
        anchors.fill: parent
        columns: 3
        rows:5
        //1st row
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
        Rectangle{
            id: topline
            width:digitlineheighthorizontal
            height:digitlinewidth
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }

            }
        }
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
        //2nd row
        Rectangle{
            id: lefttopline
            width:digitlinewidth
            height:digitlineheightvertical
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        Item{
            width:digitlineheighthorizontal
            height:digitlineheightvertical
        }
        Rectangle{
            id: righttopline
            width:digitlinewidth
            height:digitlineheightvertical
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        //3rd row
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
        Rectangle{
            id: middleline
            width:digitlineheighthorizontal
            height:digitlinewidth
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
        //4th row
        Rectangle{
            id: leftbottomline
            width:digitlinewidth
            height:digitlineheightvertical
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        Item{
            width:digitlineheighthorizontal
            height:digitlineheightvertical
        }
        Rectangle{
            id: rightbottomline
            width:digitlinewidth
            height:digitlineheightvertical
            property bool dimmed: true
            color: digitemptycolor
            radius: digitlinewidth/2
            opacity:0.1
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        //5th row
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
        Rectangle{
            id: bottomline
            width:digitlineheighthorizontal
            height:digitlinewidth
            property bool dimmed: true
            opacity:0.1
            color: digitemptycolor
            radius: digitlinewidth/2
            onDimmedChanged: {
                if(dimmed) {
                    color=digitemptycolor
                    opacity=0.25
                }
                else {
                    color=digitcolor
                    opacity=1.0
                }
            }
        }
        Item{
            width:digitlinewidth
            height:digitlinewidth
        }
    }
    onDigittodisplayChanged: {
        switch(digittodisplay) {
        case -1:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=false;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 0:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=false;
            middleline.dimmed=true;
            leftbottomline.dimmed=false;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 1:
            topline.dimmed=true;
            lefttopline.dimmed=true;
            righttopline.dimmed=false;
            middleline.dimmed=true;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=true;
            break;
        case 2:
            topline.dimmed=false;
            lefttopline.dimmed=true;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=false;
            rightbottomline.dimmed=true;
            bottomline.dimmed=false;
            break;
        case 3:
            topline.dimmed=false;
            lefttopline.dimmed=true;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 4:
            topline.dimmed=true;
            lefttopline.dimmed=false;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=true;
            break;
        case 5:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=true;
            middleline.dimmed=false;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 6:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=true;
            middleline.dimmed=false;
            leftbottomline.dimmed=false;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 7:
            topline.dimmed=false;
            lefttopline.dimmed=true;
            righttopline.dimmed=false;
            middleline.dimmed=true;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=true;
            break;
        case 8:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=false;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        case 9:
            topline.dimmed=false;
            lefttopline.dimmed=false;
            righttopline.dimmed=false;
            middleline.dimmed=false;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=false;
            bottomline.dimmed=false;
            break;
        default:
            console.log("value must be between 0..9")
            topline.dimmed=true;
            lefttopline.dimmed=true;
            righttopline.dimmed=true;
            middleline.dimmed=true;
            leftbottomline.dimmed=true;
            rightbottomline.dimmed=true;
            bottomline.dimmed=true;
            return;
        }
    }
}
