/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property int hour : 0;
    property int min : 0;
    property int sec : 0;
    Row{
        width: parent.width
        height: parent.height
        spacing: 0
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Digit{
            id:alldechourdigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.05
            height: parent.height
        }
        Digit{
            id:allhourdigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Text {
            height: parent.height
            width: page.width*0.04
            text: ":"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: Theme.highlightColor
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Digit{
            id:alldecmindigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.05
            height: parent.height
        }
        Digit{
            id:allmindigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Text {
            height: parent.height
            width: page.width*0.04
            text: ":"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: Theme.highlightColor
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Digit{
            id:alldecsecdigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.05
            height: parent.height
        }
        Digit{
            id:allsecdigit
            width: page.width*0.1
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
    }
    onSecChanged:{
        alldecsecdigit.digittodisplay=sec/10;
        allsecdigit.digittodisplay=sec%10;
    }
    onMinChanged:{
        alldecmindigit.digittodisplay=min/10;
        allmindigit.digittodisplay=min%10;
    }
    onHourChanged:{
        alldechourdigit.digittodisplay=hour/10;
        allhourdigit.digittodisplay=hour%10;
    }
}
