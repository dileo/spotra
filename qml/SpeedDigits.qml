/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/
import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property real speed:0.0;
    Row{
        width: parent.width
        height: parent.height
        spacing: 0
        Item{
            width: page.width*0.05
            height: parent.height
        }
        Digit{
            id:km1
            width:page.width*0.12
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Digit{
            id:km2
            width:page.width*0.12
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Digit{
            id:km3
            width:page.width*0.12
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.01
            height: parent.height
        }
        Text {
            height: parent.height
            width: page.width*0.01
            text: ","
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
            color: Theme.highlightColor
        }
        Item{
            width: page.width*0.01
            height: parent.height
        }
        Digit{
            id:km4
            width:page.width*0.12
            height: parent.height
            digittodisplay:0
            digitcolor: Theme.highlightColor
            digitlinewidth:8
        }
        Item{
            width: page.width*0.03
            height: parent.height
        }
        Label{
            height: parent.height
            text: "km/h"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
            color: Theme.highlightColor

        }
        Item{
            width: page.width*0.05
            height: parent.height
        }
    }
    onSpeedChanged: {
        km1.digittodisplay=speed/100
        var digit1 = km1.digittodisplay*100;
        km2.digittodisplay=(speed-digit1)/10
        var digit2 = km2.digittodisplay*10;
        km3.digittodisplay=speed-digit1-digit2
        km4.digittodisplay=(speed-digit1-digit2-km3.digittodisplay)*10
    }
}
