/*
BSD License with a blessing:
May you use this application and code for good!
May you become stronger and healthier and may you have a better life!
*/

#include "spotraengine.h"
#include <QDebug>

#define d2r 0.0174532925199433
//(3.18 / 180.0)

double haversine_km(double lat1, double long1, double lat2, double long2)
{
    double dlong = (long2 - long1) * d2r;
    double dlat = (lat2 - lat1) * d2r;
    double a = qPow(sin(dlat/2.0), 2) + qCos(lat1*d2r) * qCos(lat2*d2r) * qPow(sin(dlong/2.0), 2);
    double c = 2 * qAtan2(qSqrt(a), qSqrt(1-a));
    double d = 6367 * c;
    return d;
}

SpotraEngine::SpotraEngine(QQuickItem *parent) :
    QQuickItem(parent),
    m_positionSource(QGeoPositionInfoSource::createDefaultSource(this)),
    m_speed(0),
    m_avgspeed(0),
    m_distance(0),
    m_running(false)
{
    bool ok = false;
    if (m_positionSource) {
        ok = connect(m_positionSource, SIGNAL(positionUpdated(const QGeoPositionInfo &)), this, SLOT(positionUpdated(const QGeoPositionInfo &)));
        Q_ASSERT(ok);
        ok = connect(m_positionSource, SIGNAL(updateTimeout()), this, SLOT(positionUpdateTimeout()));
        Q_ASSERT(ok);
        //qDebug("Initialized QGeoPositionInfoSource");
    } else {
        qDebug("Failed to initialized QGeoPositionInfoSource");
    }

    m_satelliteSource = QGeoSatelliteInfoSource::createDefaultSource(this);

    if (m_satelliteSource) {
        ok = connect(m_satelliteSource, SIGNAL(satellitesInUseUpdated(const QList<QGeoSatelliteInfo> &)), this, SLOT(satellitesInUseUpdated(const QList<QGeoSatelliteInfo> &)));
        Q_ASSERT(ok);
        ok = connect(m_satelliteSource, SIGNAL(satellitesInViewUpdated(const QList<QGeoSatelliteInfo> &)), this, SLOT(satellitesInViewUpdated(const QList<QGeoSatelliteInfo> &)));
        Q_ASSERT(ok);
        //qDebug("Initialized QGeoSatelliteInfoSource");
    } else {
        qDebug("Failed to initialized QGeoSatelliteInfoSource");
    }

    if (m_positionSource) {
        m_positionSource->startUpdates();
        //qDebug("Position updates started");
    }

    if (m_satelliteSource) {
        m_satelliteSource->startUpdates();
        //qDebug("Satellite updates started");
    }
    m_xml.setAutoFormatting(true);
    //connect(&m_testTimer, SIGNAL(timeout()), this, SLOT(updateTestValues()));
    //m_testTimer.start(1000);
}

void SpotraEngine::positionUpdated(const QGeoPositionInfo& pos)
{
    /*m_latitude = pos.coordinate().latitude();
        m_longitude = pos.coordinate().longitude();
        m_altitude = pos.coordinate().altitude();
        m_time = pos.timestamp().toString();
        m_direction = QString::number(pos.attribute(QGeoPositionInfo::Direction));
        m_groundSpeed = QString::number(pos.attribute(QGeoPositionInfo::GroundSpeed));
        m_verticalSpeed = QString::number(pos.attribute(QGeoPositionInfo::VerticalSpeed));
        m_horizontalAccuracy = QString::number(pos.attribute(QGeoPositionInfo::HorizontalAccuracy));
        m_verticalAccuracy = QString::number(pos.attribute(QGeoPositionInfo::VerticalAccuracy));
        m_magneticVariation = QString::number(pos.attribute(QGeoPositionInfo::MagneticVariation));

        parseRawData();*/
    //default logging everything, not accurate because of gps artifacts
    /*if(m_running) {
        qDebug()<<__FUNCTION__<<pos.coordinate().altitude();
        qDebug()<< pos.coordinate().latitude() << pos.coordinate().longitude();
        m_latitudeList.append(pos.coordinate().latitude());
        m_longitudeList.append(pos.coordinate().longitude());
        m_speed=pos.attribute(QGeoPositionInfo::GroundSpeed)*3.6;
        m_timestampList.append(pos.timestamp());
        m_altitudeList.append(pos.coordinate().altitude());
        qDebug()<<m_speed;
        emit speedChanged();
        m_avgspeed = ((m_timestampList.count() - 1) * m_avgspeed + m_speed) / m_timestampList.count();
        emit avgspeedChanged();
        if(m_timestampList.count()>1) {
            //distance can be calculated from speed and time, maybe long and lat is not needed
            m_distance += pos.attribute(QGeoPositionInfo::GroundSpeed) * m_timestampList.at(m_timestampList.count() - 2).secsTo(m_timestampList.at(m_timestampList.count()- 1))/1000;
            emit distanceChanged();
        }
    }*/
    //the gps points are not accurate so some smoothing is needed.
    //avarage calculation making 1 point from 4
    if(m_running) {
        //Checking that none of the values are not NAN means something wrong with GPS
        if((pos.coordinate().latitude()==pos.coordinate().latitude())&&
                (pos.coordinate().longitude()==pos.coordinate().longitude())&&
                (pos.coordinate().altitude()==pos.coordinate().altitude()))
        {
            ++m_count;
            if(m_count==4) {
                m_latitudeList.append((pos.coordinate().latitude()+m_lattmp)/4);
                m_longitudeList.append((pos.coordinate().longitude()+m_longtmp)/4);
                m_altitudeList.append((pos.coordinate().altitude()+m_alttmp)/4);
                m_timestampList.append(pos.timestamp());
                //qDebug()<<pos.timestamp();
                m_lattmp=0;
                m_longtmp=0;
                m_alttmp=0;
                m_count=0;
                //ground speed is not really accurate
                /*m_speed=((m_speedtmp+pos.attribute(QGeoPositionInfo::GroundSpeed))/4)*3.6;
            m_speedtmp=0;
            emit speedChanged();
            m_avgspeed = ((m_timestampList.count() - 1) * m_avgspeed + m_speed) / m_timestampList.count();
            emit avgspeedChanged();*/
                //calculate distance from coordinates
                //Sqrt((N1-N2)²+(E1-E2)²)/1000=Distance in Kilometers
                if(m_timestampList.count()>1) {
                    //not accurate altitude change would be needed too
                    //m_distance += qSqrt(qPow(m_latitudeList.last()-m_latitudeList.at(m_latitudeList.count()-2),2)+qPow(m_longitudeList.last()-m_longitudeList.at(m_longitudeList.count()-2),2))/1000;
                    //emit distanceChanged();
                    double disttmp = haversine_km(m_latitudeList.last(),m_longitudeList.last(),
                                                  m_latitudeList.at(m_latitudeList.count()-2),m_longitudeList.at(m_longitudeList.count()-2));
                    m_distance += disttmp;
                    //qDebug()<<m_distance;
                    emit distanceChanged();
                    //qDebug()<<m_timestampList.last().toMSecsSinceEpoch()-
                    //          m_timestampList.at(m_timestampList.count()-2).toMSecsSinceEpoch();//4000
                    //qDebug()<<(m_timestampList.last().toMSecsSinceEpoch()-
                    //                                     m_timestampList.at(m_timestampList.count()-2).toMSecsSinceEpoch())/3600000;
                    m_speed=(disttmp/(m_timestampList.last().toMSecsSinceEpoch()-
                                      m_timestampList.at(m_timestampList.count()-2).toMSecsSinceEpoch()))*3600000;
                    //qDebug()<<m_speed;
                    //qDebug()<<"--------------------------------";
                    //m_speedtmp=0;
                    emit speedChanged();
                    m_avgspeed = ((m_timestampList.count() - 1) * m_avgspeed + m_speed) / m_timestampList.count();
                    emit avgspeedChanged();
                }
            }
            else {
                m_lattmp+=pos.coordinate().latitude();
                m_longtmp+=pos.coordinate().longitude();
                m_alttmp+=pos.coordinate().altitude();
                m_speedtmp+=pos.attribute(QGeoPositionInfo::GroundSpeed);
            }
        }
        /*if(m_timestampList.count()>1) {
            //distance can be calculated from speed and time, maybe long and lat is not needed
            m_distance += pos.attribute(QGeoPositionInfo::GroundSpeed) * m_timestampList.at(m_timestampList.count() - 2).secsTo(m_timestampList.at(m_timestampList.count()- 1))/1000;
            emit distanceChanged();
        }*/
    }
}

void SpotraEngine::satellitesInUseUpdated(const QList<QGeoSatelliteInfo> &satellites)
{
    m_gpsstrength = satellites.count();
    emit gpsstrengthChanged();
}

void SpotraEngine::satellitesInViewUpdated(const QList<QGeoSatelliteInfo> &satellites)
{
    Q_UNUSED(satellites)
}

void SpotraEngine::positionUpdateTimeout()
{
    //qDebug("positionUpdateTimeout() received");
}

int SpotraEngine::gpsstrength() const
{
    return m_gpsstrength;
}

qreal SpotraEngine::speed() const
{
    return m_speed;
}

qreal SpotraEngine::avgspeed() const
{
    return m_avgspeed;
}

qreal SpotraEngine::distance() const
{
    return m_distance;
}

void SpotraEngine::updateTestValues()
{
    m_speed+=11.11;
    if(m_speed>999.9)
        m_speed=0;
    m_avgspeed+=11.11;
    if(m_avgspeed>999.9)
        m_avgspeed=0;
    m_distance+=111.11;
    if(m_distance>999.9)
        m_distance=0;
    emit speedChanged();
    emit avgspeedChanged();
    emit distanceChanged();
}

void SpotraEngine::startGPS()
{
    m_running = true;
    m_count = 0;
    m_lattmp = 0;
    m_longtmp = 0;
    m_alttmp = 0;
    m_speedtmp = 0;
    m_restarttime = 0;
    m_starttime = QDateTime::currentMSecsSinceEpoch();
    m_longitudeList.clear();
    m_latitudeList.clear();
    m_timestampList.clear();
    m_altitudeList.clear();
    m_lapindexList.clear();
}

void SpotraEngine::restartGPS()
{
    m_running = true;
    m_starttime = QDateTime::currentMSecsSinceEpoch();
}

void SpotraEngine::stopGPS()
{
    m_running = false;
}

void SpotraEngine::pauseGPS()
{
    m_restarttime = QDateTime::currentMSecsSinceEpoch()-m_starttime+m_restarttime;
    m_running = false;
    m_lapindexList.append(m_timestampList.count());
}

void SpotraEngine::saveGPX()
{
    QString filename(QDir::homePath()+"/Documents/Spotra-");
    filename.append(QDateTime::currentDateTime().toString("dd_MM_yyyy_hh_mm_ss"));
    filename.append(".gpx");
    QFile xmlfile(filename);
    if (!xmlfile.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    m_xml.setDevice(&xmlfile);

    m_xml.writeStartDocument();
    //m_xml.writeDTD("<!DOCTYPE xbel>");
    m_xml.writeStartElement("gpx");
    m_xml.writeAttribute("xsi:schemaLocation","http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd");
    m_xml.writeAttribute("version", "1.1");
    m_xml.writeAttribute("creator","SpoTra");
    m_xml.writeAttribute("xmlns","http://www.topografix.com/GPX/1/1");
    m_xml.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
    m_xml.writeAttribute("xmlns:gpxtpx","http://www.garmin.com/xmlschemas/TrackPointExtension/v1");
    m_xml.writeStartElement("trk");
    m_xml.writeStartElement("trkseg");
    /*    <trkpt lat="52.3731742136" lon="4.9113854976">
        <ele>57.0</ele>
        <time>2014-11-17T07:05:55.398Z</time>
    </trkpt>*/
    int lapindex=0;
    for (int i=0; i<m_latitudeList.count(); i++) {
        if ((m_lapindexList.count()>0)&&(m_lapindexList.count()>lapindex)) {
            if(m_lapindexList.at(lapindex)==i) {
                m_xml.writeEndElement();//trkseq
                m_xml.writeStartElement("trkseg");
                ++lapindex;
            }
        }
        m_xml.writeStartElement("trkpt");
        m_xml.writeAttribute("lat", QString::number(m_latitudeList.at(i),'f'));
        m_xml.writeAttribute("lon", QString::number(m_longitudeList.at(i),'f'));
        m_xml.writeTextElement("ele",QString::number(m_altitudeList.at(i),'f'));
        m_xml.writeTextElement("time",QDateTime(m_timestampList.at(i)).toString(Qt::ISODate));
        m_xml.writeEndElement();//trkpt
    }
    m_xml.writeEndElement();//trkseq
    m_xml.writeEndElement();//trk
    m_xml.writeEndElement();//gpx
    m_xml.writeEndDocument();
    xmlfile.close();
    m_latitudeList.clear();
    m_longitudeList.clear();
    m_altitudeList.clear();
}

int SpotraEngine::getSecs()
{
    return m_secs;
}

int SpotraEngine::getMins()
{
    return m_mins;
}

int SpotraEngine::getHours()
{
    return m_hours;
}

void SpotraEngine::trackTime()
{
    int difftime = QDateTime::currentMSecsSinceEpoch() - m_starttime + m_restarttime;
    m_hours=qFloor(difftime/3600000);
    m_mins=qFloor((difftime-(m_hours*3600000))/60000);
    m_secs=qFloor((difftime-(m_hours*3600000)-(m_mins*60000))/1000);
}
